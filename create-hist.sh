#!/bin/bash
#Usage
# ./create-hist.sh PATH_TO_TRACE PATH_TO_DIRECTORY_OUTPUT
mkdir offsets
mkdir hists
mkdir hists/hist1
mkdir hists/hist2
mkdir hists/hist3

for j in $(seq 0 7);
do
    mkdir hists/hist1/off$j
    mkdir hists/hist2/off$j
    mkdir hists/hist3/off$j
    for i in $(seq 65 118);
    do
        grep " $j $i\$" offset_$j > offset_$j\_nbhits_$i
        cat offset_$j\_nbhits_$i | awk -v colonne=1 -v largeur=1 -f hist.awk > hist_1_off_$j\_nhits_$i
        cat offset_$j\_nbhits_$i | awk -v colonne=2 -v largeur=1 -f hist.awk > hist_2_off_$j\_nhits_$i
        cat offset_$j\_nbhits_$i | awk -v colonne=3 -v largeur=1 -f hist.awk > hist_3_off_$j\_nhits_$i

        mv hist_1_off_$j\_nhits_$i hists/hist1/off$j
        mv hist_2_off_$j\_nhits_$i hists/hist2/off$j
        mv hist_3_off_$j\_nhits_$i hists/hist3/off$j

        mv offset_$j\_nbhits_$i offsets
    done
done
