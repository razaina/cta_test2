#!/bin/bash
xrange1=$1
xrange2=$2

mkdir img
rm img/*.png
for k in $(seq 1 3);
do
    for j in $(seq 0 7);
    do
        for i in $(seq 65 118);
        do
            trace="hist$k/off$j/hist_$k\_off_$j\_nhits_$i"
            ./disp.sh "[$1:$2]" $trace $i $j
        done
        mkdir hist$k/off$j/$1\_$2
        mv hist$k/off$j/*.png hist$k/off$j/$1\_$2
        convert -delay 50 hist$k/off$j/$1\_$2/*.png -loop 0 hist$k/off$j/$1\_$2/animated.gif
    done
done

