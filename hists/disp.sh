#!/bin/bash
x_range=$1
trace=$2
nbhits=$3
offset=$4
gnuplot <<- EOF
    set xrange $x_range
    set term png size 700, 400 
    set output "$trace.png"
    set xlabel "Nb. Cycles" 
    set ylabel "Frequency"
    set grid
    set style fill solid border -1
    plot "$trace" w boxes t "NbHits = $nbhits, Offset = $offset" 
EOF

